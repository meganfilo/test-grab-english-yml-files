# Test - Grab English YML Files



## Getting started

1. Clone project
2. Run `npm install` in project directory
3. Run `npm run dev` to start up the server. If Nodemon did not install successfully, run `npm install nodemon -g` and then `npm run dev` again.
4. Visit `localhost:3000/api/v1?id=[some kind of merge request ID from Buyer Experience]` in the browser or using something like Postman.

