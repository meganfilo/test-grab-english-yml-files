const express = require("express");
const axios = require("axios");
var yaml = require("js-yaml");

const app = express();
const port = 3000;

app.get("/api/v1", (req, res) => {
  // 2384
  const mergeRequestId = req.query.id;
  let mergeRequestInternalId;
  let targetBranch;
  let mergeRequestTitle;
  let mergeRequestURL;
  let filePaths = [];

  // Step 1: Grab MR information by the MR ID. This ID will eventually get passed to the API by the Webhook that runs at an MR merge event
  axios
    .get(
      `https://gitlab.com/api/v4/projects/28847821/merge_requests/${mergeRequestId}`
    )
    .then((response) => {
      mergeRequestInternalId = response.data.iid;
      targetBranch = response.data.target_branch;
      mergeRequestTitle = response.data.title;
      mergeRequestURL = response.data.web_url;

      // only move forward if the target was Main
      if (targetBranch === "main") {
        // Step 2: Use the MR Internal ID to grab the MR diffs, collecting the raw data of the English yaml files.
        return axios
          .get(
            `https://gitlab.com/api/v4/projects/28847821/merge_requests/${mergeRequestInternalId}/diffs`
          )
          .then((response) => {
            return response.data.forEach((change) => {
              if (
                change.new_path.includes(".yml") &&
                !change.deleted_file &&
                !change.new_path.includes("fr-fr") &&
                !change.new_path.includes("de-de") &&
                !change.new_path.includes("ja-jp")
              ) {
                // The Raw File GitLab API call needs URLs encoded
                // replace '/' with '%2'
                filePaths.push(encodeURIComponent(change.new_path));
              } else res.end;
            });
          });
      } else {
        return res.end;
      }
    })
    .then((response) => {
      // Step 3: Make a POST request to Argo using their JSON body requirements
      // don't forget to build the actual yaml files from the raw file data we stored in step 2 (yaml.safeDump(parsedData);)
    })

    .catch((error) => {
      console.log(error);
    });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
